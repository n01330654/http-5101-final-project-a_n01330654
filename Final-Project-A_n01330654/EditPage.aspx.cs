﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project_A_n01330654
{
    public partial class EditPage : System.Web.UI.Page
    {
        //I use Christine's In-Class Example as Reference and guide to help me acheive this functionality of this page
        public int pageid
        {
             get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo(pageid);

            if (pagerow == null)
            {
                page_title_header.InnerHtml = "No Page Found.";
                return;
            }

            page_title.Text = pagerow["pagetitle"].ToString();
            page_content.Text = pagerow["pagecontent"].ToString();

            if (!Page.IsPostBack)
            {
                page_title_header.InnerHtml += page_title.Text;

            }
        }
        //I use Christine's In-Class Example as Reference and guide to help me acheive this functionality of this page
        protected void EditPage_Click(object sender, EventArgs e)
        {
              string pageTitle = page_title.Text;
              string pageContent = page_content.Text;


              string basequery = "Update pages set pagetitle='" + pageTitle +"'," +
              "pagecontent='" + pageContent + "' where pageid=" + pageid;

              pages_query.InnerHtml = basequery;
              edit_page.UpdateCommand = basequery;
              edit_page.Update();

        }
        //I use Christine's In-Class Example as Reference and guide to help me acheive this functionality of this page
        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid.ToString();
            page_select.SelectCommand = query;

            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }

            DataRowView pagerowview = pageview[0];
            return pagerowview;

        }


    }
}