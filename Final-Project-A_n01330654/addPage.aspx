﻿<%@ Page Title="Add Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="addPage.aspx.cs" Inherits="Final_Project_A_n01330654.addPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:SqlDataSource runat="server" id="insert_page"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>

    <div class="inputContainer">
    <h2>New Page</h2>
    </div>    

    <div class="inputContainer">
        <div>
        <label>Page title: </label>
        </div>
        <asp:textbox id="page_title" runat="server" cssClass="inputTextbox" placeholder="Enter Page Title">
        </asp:textbox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Page Title" ControlToValidate="page_title" ID="validatePageTitle"></asp:RequiredFieldValidator>
    </div>

    <div class="inputContainer">
        <div>
        <label>Page Content</label>
        </div>
        <asp:textbox id="page_content" runat="server"  placeholder="Please Enter your Content here..." Columns="1000" Rows="10" TextMode="multiline" class="textarea">
        </asp:textbox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Page Content" ControlToValidate="page_content" ID="validatePageContent"></asp:RequiredFieldValidator>
    </div>
       
    <div class="inputContainer">
        <div>
        <label>Select Author</label>
        </div>
        <div>
        <asp:DropDownList ID="author_list" runat="server">
        <asp:ListItem value="John Doe" Text="John Doe"></asp:ListItem>
        <asp:ListItem value="Sam Smith" Text="Sam Smith"></asp:ListItem>
        <asp:ListItem value="Uzumaki Naruto" Text="Uzumaki Naruto"></asp:ListItem>
        </asp:DropDownList>
        </div>
    </div>
    
    <div class="inputContainer">
    <div>
    <asp:Button Text="Publish" runat="server" OnClick="Publish_Click" cssClass="inputButton"/>
    <asp:Button Text="Save as Draft" runat="server" OnClick="Unpublish_Click" cssClass="inputButton"/>
    </div>
    
    <div class="inputContainer">
    <h3 ><a href="ManagePages.aspx">Go Back to Main Page</a></h3>
    </div>

    <div class="inputContainer">
    <div id="pages_query" class="querybox" runat="server"></div>
    </div>
</asp:Content>
