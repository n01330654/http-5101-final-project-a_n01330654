﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project_A_n01330654
{
    public partial class Page : System.Web.UI.Page
    {
        //I use Christine's In-Class Example as Reference and guide to help me acheive this functionality of this page
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }

        public string page_status
        {
            get { return Request.QueryString["page_status"]; }
        }
    
        private string page_basequery = "SELECT pageid as 'pageid', pagetitle as 'Page Title', " +
            "pagecontent as'Page Content', convert(varchar,date_created,107) as 'date_created' FROM PAGES";
     

        protected void Page_Load(object sender, EventArgs e)
        {
            //I use Christine's In-Class Example as Reference and guide to help me acheive this functionality of this page
            if (pageid == "" || pageid == null || page_status == "Unpublished" ) page_title.InnerHtml = "No Page found."; 
            else
            {

                page_basequery += " WHERE pageid = " + pageid;
                pages_select.SelectCommand = page_basequery;
                pages_query.InnerHtml = page_basequery;


                DataView pageview = (DataView)pages_select.Select(DataSourceSelectArguments.Empty);

                //I use Christine's In-Class Example as Reference and guide to help me acheive this functionality of this page
                string pagetitle = pageview[0]["Page Title"].ToString();
                page_title.InnerHtml = pagetitle;
                string pagecontent = pageview[0]["Page Content"].ToString();
                page_content.InnerHtml = pagecontent;
                string publishdate = pageview[0]["date_created"].ToString();
                published_date.InnerHtml = "Published Date: " + publishdate;

            }
        }
    }    
}