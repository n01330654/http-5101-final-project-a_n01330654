﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project_A_n01330654
{
    public partial class addPage : System.Web.UI.Page
    {
        //I use Christine's In-Class Example as Reference and guide to help me acheive this functionality of this page

        private string basequery = "INSERT into pages " +
            "(pagetitle, pagecontent , page_author , page_status ,date_created)" +
            " VALUES ";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Publish_Click(object sender, EventArgs e)
        {
            string pageTitle = page_title.Text;
            string pageContent = page_content.Text;
            string authorPick = author_list.SelectedValue;
            string now = DateTime.Today.ToString("dd/MM/yyyy");
            string page_status = "Published";


            basequery += "('" + pageTitle + "', '" + pageContent + "','" + authorPick + "','" + page_status + "',convert(DATETIME,'" + now + "',103))";

            insert_page.InsertCommand = basequery;
            insert_page.Insert();
            pages_query.InnerHtml = basequery;
            Response.Redirect(Request.Url.AbsoluteUri);
        }

        protected void Unpublish_Click(object sender, EventArgs e)
        {
            string pageTitle = page_title.Text;
            string pageContent = page_content.Text;
            string authorPick = author_list.SelectedValue;
            string date_created = DateTime.Today.ToString("dd/MM/yyyy");
            string page_status = "Unpublished";

            basequery += "('" + pageTitle + "', '" + pageContent + "','" + authorPick + "','" + page_status + "',convert(DATETIME,'" + date_created + "',103))";

            insert_page.InsertCommand = basequery;
            insert_page.Insert();
            pages_query.InnerHtml = basequery;

        }
    }
}