﻿<%@ Page Title="Manage Pages" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManagePages.aspx.cs" Inherits="Final_Project_A_n01330654.ManagePages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="inputContainer">
    <div id="manage-page-row">
        <h2><%: Title %></h2>
        <h3 ><a href="addPage.aspx">Add Page</a></h3>
    </div>
    </div>
    
    <div class="inputContainer">
    <div id="search-container">
        <asp:TextBox runat="server" ID="page_search" cssClass="inputTextbox" placeholder="Search for Page Tile"></asp:TextBox>
    </div>
    <div>
        </br>
        <asp:Button ID="searchButton" runat="server" Text="Search" OnClick="Search_Pages" cssClass="inputButton"/>
    </div>
    </div>
    
    <asp:SqlDataSource runat="server" 
        ID="pages_select" SelectCommand="select * from pages" ConnectionString="<%$ connectionStrings:final_sql_con %>">
    </asp:SqlDataSource>

     <asp:SqlDataSource runat="server"
        ID="del_page"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>

    <div class="inputContainer">
    <asp:DataGrid id="pages_list"
        runat="server" AutoGenerateColumns="False" OnItemCommand="ManagePages_ItemCommand">
    </asp:DataGrid>
    </div>

    <div class="inputContainer">
    <div id="pages_query" class="querybox" runat="server"></div>
    </div>

</asp:Content>
