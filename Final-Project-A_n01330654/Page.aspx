﻿<%@ Page Title="View Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="Final_Project_A_n01330654.Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        
        <div class="inputContainer">
        <h2 id="page_title" runat="server"></h2>
        <h3><a href="EditPage.aspx?pageid=<%Response.Write(this.pageid);%>">Edit This Page</a></h3>
        <ASP:ThemeOptions ID="ThemeOptions" runat="server" tagName="ThemeOptions"></ASP:ThemeOptions>
        </div>

        <div class="inputContainer">
        <h4 id="page_content_label" runat="server">Below is the page content:</h4>
        <p id="page_content" runat="server"></p>
        <br/>

        <p id="published_date" runat="server" class="published-date"></p>
        <h5><a href="ManagePages.aspx">Go Back to Main Page</a></h5>
        </div>

   <asp:SqlDataSource runat="server"
        id="pages_select"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>
        

        <div class="inputContainer">
        <div id="pages_query" class="querybox" runat="server"></div>
        </div>

</asp:Content>
