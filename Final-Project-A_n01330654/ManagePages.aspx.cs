﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Final_Project_A_n01330654
{
    public partial class ManagePages : System.Web.UI.Page
    {
        //I use Christine's In-Class Example as Reference and guide to help me acheive this functionality of this page

        private string basequery = "SELECT pageid, pagetitle as 'Page Title', page_author as 'Author' , convert(varchar,date_created,103) as 'Date Created', page_status as'Page Status' FROM PAGES";


        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = basequery;
            pages_list.DataSource = pages_select;

            pages_query.InnerHtml = basequery;


            BoundColumn pageid = new BoundColumn();
            pageid.DataField = "pageid";
            pageid.HeaderText = "ID";
            pageid.Visible = false;
            pages_list.Columns.Add(pageid);

            BoundColumn title = new BoundColumn();
            title.DataField = "Page Title";
            title.HeaderText = "Page Title";
            pages_list.Columns.Add(title);

            BoundColumn author = new BoundColumn();
            author.DataField = "Author";
            author.HeaderText = "Author";
            pages_list.Columns.Add(author);

            BoundColumn dateCreated = new BoundColumn();
            dateCreated.DataField = "Date Created";
            dateCreated.HeaderText = "Date Created";
            pages_list.Columns.Add(dateCreated);

            BoundColumn pageStatus = new BoundColumn();
            pageStatus.DataField = "Page Status";
            pageStatus.HeaderText = "Page Status";
            pages_list.Columns.Add(pageStatus);

            BoundColumn edit = new BoundColumn();
            edit.DataField = "Edit";
            edit.HeaderText = "Action";
            pages_list.Columns.Add(edit);

            ButtonColumn delpage = new ButtonColumn();
            delpage.HeaderText = "Delete";
            delpage.Text = "Delete";
            delpage.ButtonType = ButtonColumnType.PushButton;
            delpage.CommandName = "DelPage";
            pages_list.Columns.Add(delpage);

            pages_list.DataSource = Pages_Manual_Bind(pages_select);
            pages_list.DataBind();
        }

        protected void ManagePages_ItemCommand(object sender, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "DelPage")
            {
                int pageid = Convert.ToInt32(e.Item.Cells[0].Text);
                DelPage(pageid);
            }

        }

        protected void DelPage(int pageid)
        {
            string basequery = "DELETE FROM PAGES WHERE pageid=" + pageid.ToString();
            del_page.DeleteCommand = basequery;
            del_page.Delete();

            pages_query.InnerHtml = basequery;

        }

        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {
            //I use Christine's In-Class Example as Reference and guide to help me acheive this functionality of this page
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            DataColumn editcol = new DataColumn();
            editcol.ColumnName = "Edit";

            mytbl.Columns.Add(editcol);

            foreach (DataRow row in mytbl.Rows)
            {
                //I use Christine's In-Class Example as Reference and guide to help me acheive this functionality of this page
                row["Page Title"] =
                    "<a href=\"Page.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["Page Title"]
                    + "</a>";

                row[editcol] = "<a href=\"EditPage.aspx?pageid=" +
                    row["pageid"] + "\">Edit</a>";

            }

            myview = mytbl.DefaultView;

            return myview;
        }

        protected void Search_Pages(object sender, EventArgs e)
        {
            //I use Christine's In-Class Example as Reference and guide to help me acheive this functionality of this page
            string newsql = basequery + " WHERE (1=1) ";
            string keyword = page_search.Text;

            if (keyword != "")
            {
                newsql +=
                    " AND pagetitle LIKE '%" + keyword + "%'";
            }
        

            pages_select.SelectCommand = newsql;
            pages_query.InnerHtml = newsql;

            pages_list.DataSource = Pages_Manual_Bind(pages_select);
            pages_list.DataBind();

        }

    }
}