﻿<%@ Page Title="Edit Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="Final_Project_A_n01330654.EditPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:SqlDataSource runat="server" 
        ID="page_select"  ConnectionString="<%$ connectionStrings:final_sql_con %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server" id="edit_page"
     ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>

    <div class="inputContainer">
    <h2 runat="server" id="page_title_header">Edit </h2>
    </div>

    <div class="inputContainer">
        <div>
        <label>Page title: </label>
        </div>
        <asp:textbox id="page_title" runat="server" cssClass="inputTextbox">
        </asp:textbox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Page Title" ControlToValidate="page_title" ID="validatePageTitle"></asp:RequiredFieldValidator>
    </div>
    
    <div class="inputContainer">
        <label>Page Content</label>
        <br>
        <br>
        <asp:textbox id="page_content" runat="server" Columns="1000" Rows="10" TextMode="multiline" width="1200px" class="textarea">
        </asp:textbox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Page Content" ControlToValidate="page_content" ID="validatePageContent"></asp:RequiredFieldValidator>
    <div>
        <asp:Button Text="Edit Page" runat="server" OnClick="EditPage_Click" cssClass="inputButton" />
    </div>
        <h3><a href="ManagePages.aspx">Go Back to Main Page</a></h3>
    </div>
    
    <div class="inputContainer">
    <div id="pages_query" class="querybox" runat="server"></div>
    </div>

</asp:Content>
