﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project_A_n01330654.usercontrols
{
    public partial class Menu_List : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            string basequery = "select * from pages";
            menu_generate.SelectCommand = basequery;


            DataView mytbl = (DataView)menu_generate.Select(DataSourceSelectArguments.Empty);

            string MenuList = "";

            foreach (DataRow row in mytbl.ToTable().Rows)
            {
                string title = row["pagetitle"].ToString();
                string pageid = row["pageid"].ToString();

                MenuList += "<li><a href =\"Page.aspx?pageid=" + pageid + "\">" + title + "</a></li>";

            }
            menu_container.InnerHtml = MenuList;

        }

        
    }
}