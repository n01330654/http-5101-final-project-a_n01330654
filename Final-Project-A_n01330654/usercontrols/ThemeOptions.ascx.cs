﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project_A_n01330654.usercontrols
{
    public partial class ThemeOptions : System.Web.UI.UserControl
    {
        // I found this source code online but still not working as I Attempt to do this feature
        //www. youtube dotcom/watch?v=7RwgXBV-v-0 -->

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Request.QueryString["Theme"] != null)
            {
                Page.Theme = Request.QueryString["Theme"].ToString();
            }
                
        }

        protected void ThemeOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = themeOptions.SelectedValue;
            Response.Redirect("~/Page.aspx?Theme=" + name);
        }


    }
}