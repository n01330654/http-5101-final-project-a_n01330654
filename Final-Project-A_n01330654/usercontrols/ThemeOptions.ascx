﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ThemeOptions.ascx.cs" Inherits="Final_Project_A_n01330654.usercontrols.ThemeOptions" %>

<div>
    <h3>Select your page Theme : </h3>
        <asp:DropDownList ID="themeOptions" runat="server" AutoPostBack="True" OnTextChanged="ThemeOption_SelectedIndexChanged" Width="175px">
                <asp:ListItem >Select A Theme</asp:ListItem>
                <asp:ListItem value="Default Theme">Default Theme</asp:ListItem>
                <asp:ListItem value="Light Theme">Light Theme</asp:ListItem>
            </asp:DropDownList>
            <br />
      </div>
